import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsPageCarouselRoutingModule } from './details-page-carousel-routing.module';
import { DetailsPageCarouselComponent } from './details-page-carousel.component';


@NgModule({
    declarations: [DetailsPageCarouselComponent],
    imports: [
        CommonModule,
        DetailsPageCarouselRoutingModule
    ],
    exports: [
        DetailsPageCarouselComponent
    ]
})
export class DetailsPageCarouselModule { }
