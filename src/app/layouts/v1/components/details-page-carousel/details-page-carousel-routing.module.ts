import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsPageCarouselComponent } from './details-page-carousel.component';

const routes: Routes = [{ path: '', component: DetailsPageCarouselComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsPageCarouselRoutingModule { }
