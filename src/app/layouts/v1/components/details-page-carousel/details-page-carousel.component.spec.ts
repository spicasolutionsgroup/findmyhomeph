import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsPageCarouselComponent } from './details-page-carousel.component';

describe('DetailsPageCarouselComponent', () => {
  let component: DetailsPageCarouselComponent;
  let fixture: ComponentFixture<DetailsPageCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsPageCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsPageCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
