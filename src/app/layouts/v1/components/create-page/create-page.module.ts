import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreatePageRoutingModule } from './create-page-routing.module';
import { CreatePageComponent } from './create-page.component';

import { CreateHeaderModule } from '../create-header/create-header.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [CreatePageComponent],
    imports: [
        CommonModule,
        CreatePageRoutingModule,
        CreateHeaderModule,

        ReactiveFormsModule
    ],
    exports: [
        CreatePageComponent
    ]
})
export class CreatePageModule { }
