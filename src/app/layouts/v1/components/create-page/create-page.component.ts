import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { S3Client, PutObjectCommand, PutObjectAclCommand, S3ClientConfig } from '@aws-sdk/client-s3';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
    selector: 'app-create-page',
    templateUrl: './create-page.component.html',
    styleUrls: ['./create-page.component.css']
})
export class CreatePageComponent implements OnInit {

    s3: S3Client = null;
    s3Config: S3ClientConfig = {};
    bucketName: string;
    bucketUrl: string;
    uploadParams = {};
    selectedFiles = [];
    imageFiles = [];
    formValue = {};

    listingForm: FormGroup;
    sizeLimit: number = 10485760; // 10 MB
    imageCount = 3;

    uploading: boolean = false;
    subRec: Subscription;

    constructor(
        private fb: FormBuilder,
        private svcApi: ApiService,
        private svcUI: UiService
    ) {}

    ngOnInit(): void {
        this.initForm();

        this.bucketName = environment.AWSConfig.bucketName;
        this.bucketUrl = environment.AWSConfig.bucketUrl;
        this.s3Config = {
            credentials: {
                accessKeyId: environment.AWSConfig.accessKeyId,
                secretAccessKey: environment.AWSConfig.secretAccessKey
            },
            region: environment.AWSConfig.region
        };

        this.s3 = new S3Client(this.s3Config);
    }

    ngOnDestroy(): void {
        if (this.subRec) {
            this.subRec.unsubscribe();
        }
    }

    initForm() {
        let rxNumbers = '^(0|[1-9][0-9]*)$';

        this.listingForm = this.fb.group({
            homeType: ['', Validators.required],
            address: this.fb.group({
                address_1: ['', Validators.required],
                address_2: [''],
                city: ['', Validators.required],
                province: ['', Validators.required]
            }),
            floorArea: ['',
                [
                    Validators.pattern(rxNumbers),
                    Validators.min(0),
                    Validators.max(10000)
                ]
            ],
            beds: ['', [
                Validators.required
            ] ],
            baths: ['', Validators.required],
            parking: ['', Validators.required],
            floors: [''],
            maxOcc: [''],
            furnishing: [''],
            allowPets: [''],
            allowSmoking: [''],
            notes: [''],
            pricing: this.fb.group({
                price: ['', Validators.required ],
                monthsdeposit: [''],
                monthsadvance: [''],
                acceptCash: [''],
                acceptPDC: ['']
            }),
            images: this.fb.array([])
        });

        this.listingForm.valueChanges.subscribe(console.log);
    }

    get listingImages() {
        return this.listingForm.get('images') as FormArray;
    }

    addImage() {
        const image = this.fb.group({
            image: [],
            isCover: []
        });

        this.listingImages.push(image);
        console.log(this.listingImages);
    }

    removeImage(i) {
        this.listingImages.removeAt(i);
    }

    getFile(i, el) {
        console.log(el.files);
        if (el.files.length > 0) {
            if (el.files[0].size > this.sizeLimit) {
                alert('File too large');
                el.value = "";
                return;
            }

            const file: File = el.files[0];
            console.log(file);
            this.selectedFiles[i] = file;
        }

        console.log(this.selectedFiles);
    }

    submitForm() {
        console.log(`submitForm()`);
        this.formValue = this.listingForm.value;
        console.log(this.formValue);

        this.saveListing();
    }

    async saveListing() {
        console.log('saveListing');
        this.uploading = true;
        try {
            // Remove 'images' from the listing object
            delete this.listingForm.value['images'];

            this.prepareImages();
            let obj = this.listingForm.value;
            obj.images = [ { url: this.imageFiles[0].url } ];

            // Post the listing and then upload the images
            this.subRec = this.svcApi.post('create', 'listing', null, obj).subscribe((res) => {
                console.log(res);
                this.uploadImages(res.insertedId);
            });
        }
        catch (ex) {
            console.log(`ex: ${ex}`);
        }
    }

    prepareImages() {
        for (var k in this.selectedFiles) {
            if (this.selectedFiles.hasOwnProperty(k)) {
                console.log(`k = ${k}`);
                let fl = this.selectedFiles[k];
                console.log(fl);
                
                const dt = Date.now();
                const arrname = fl.name.split('.');
                const ext = arrname[arrname.length - 1];
                const flname = `${dt}.${ext}`.toLocaleLowerCase();
                console.log(`dt = ${dt}, ext = ${ext}, flname = ${flname}`);

                this.imageFiles.push({
                    file: fl,
                    origName: fl.name,
                    name: flname,
                    url: `${this.bucketUrl}${flname}`,
                    index: k
                });
            }
        }
}

    async uploadImages(id) {
        this.uploading = true;
        try {
            let obj = [], resPut, resPutAcl;

            for (let i = 0, n = this.imageFiles.length; i < n; i++) {
                console.log(`i = ${i}`);
                let fl = this.imageFiles[i];
                console.log(fl);
                
                const dt = Date.now();
                const arrname = fl.name.split('.');
                const ext = arrname[arrname.length - 1];
                const flname = `${dt}.${ext}`.toLocaleLowerCase();
                console.log(`dt = ${dt}, ext = ${ext}, flname = ${flname}`);

                resPut = await this.s3.send(
                    new PutObjectCommand({
                        Bucket: this.bucketName,
                        Key: flname,
                        Body: fl
                    })
                );

                let status = resPut['$metadata'].httpStatusCode;
                console.log(`resPut status = ${status}`);

                if (status == 200) {
                    resPutAcl = await this.s3.send(
                        new PutObjectAclCommand({
                            Bucket: this.bucketName,
                            Key: flname,
                            ACL: 'public-read'
                        })
                    );

                    status = resPutAcl['$metadata'].httpStatusCode;
                    console.log(`resPutAcl status = ${status}`);

                    obj.push({
                        origName: fl.name,
                        name: flname,
                        url: `${this.bucketUrl}${flname}`,
                        listing: id,
                        index: i
                    });
                };
            }

            if (obj.length > 0) {
                console.log('saving listing images to DB...');
                try {
                    this.subRec = this.svcApi.post('create', 'images', null, obj).subscribe((res) => {
                        console.log(res);
                        
                        this.svcUI.navigateTo(`/v1/listing`, { id: id });
                    });
                }
                catch (ex) {
                    console.log(`ex: ${ex}`);
                }
            }

        }
        catch (ex) {
            console.log(`ex: ${ex}`);
        }
    }

}
