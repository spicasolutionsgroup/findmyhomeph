import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingSummaryRoutingModule } from './listing-summary-routing.module';
import { ListingSummaryComponent } from './listing-summary.component';


@NgModule({
    declarations: [ListingSummaryComponent],
    imports: [
        CommonModule,
        ListingSummaryRoutingModule
    ],
    exports: [
        ListingSummaryComponent
    ]
})
export class ListingSummaryModule { }
