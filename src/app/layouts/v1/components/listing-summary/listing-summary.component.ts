import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../services/data.service';

@Component({
    selector: 'app-listing-summary',
    templateUrl: './listing-summary.component.html',
    styleUrls: ['./listing-summary.component.css']
})
export class ListingSummaryComponent implements OnInit {

    rec = null;

    constructor(
        private svcData: DataService
    ) { }

    getTotalDeposit() {
        console.log(`getTotalDeposit`);
        let sum = 0;
        let price = Number(this.rec.pricing.price);
        if (this.rec.pricing.monthsdeposit) {
            sum += (price * this.rec.pricing.monthsdeposit);
        }

        if (this.rec.pricing.monthsadvance) {
            sum += (price * this.rec.pricing.monthsadvance);
        }

        console.log(`sum = ${sum}`);
        this.rec.totaldeposit = sum;
    }

    getPaymentModes() {
        console.log(`getPaymentModes`);
        let vals = [];
        if (this.rec.pricing.acceptPDC == true) {
            vals.push('PDC');
        }

        if (this.rec.pricing.acceptCash == true) {
            vals.push('Cash');
        }

        if (this.rec.pricing.acceptCC == true) {
            vals.push('Credit Card');
        }
        this.rec.paymentModes = vals.join('/');
    }

    ngOnInit(): void {
        console.log('Listing Summary ngOnInit');
        this.rec = this.svcData.getObject('listing');
        console.log(this.rec);

        if (this.rec) {
            this.getTotalDeposit();
            this.getPaymentModes();
        }
    }

}
