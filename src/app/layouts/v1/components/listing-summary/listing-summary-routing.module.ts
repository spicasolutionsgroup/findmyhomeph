import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingSummaryComponent } from './listing-summary.component';

const routes: Routes = [{ path: '', component: ListingSummaryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingSummaryRoutingModule { }
