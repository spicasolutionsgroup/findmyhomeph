import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingNotesRoutingModule } from './listing-notes-routing.module';
import { ListingNotesComponent } from './listing-notes.component';


@NgModule({
  declarations: [ListingNotesComponent],
  imports: [
    CommonModule,
    ListingNotesRoutingModule
  ],
  exports: [
      ListingNotesComponent
  ]
})
export class ListingNotesModule { }
