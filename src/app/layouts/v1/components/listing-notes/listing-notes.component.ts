import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-listing-notes',
    templateUrl: './listing-notes.component.html',
    styleUrls: ['./listing-notes.component.css']
})
export class ListingNotesComponent implements OnInit {

    rec = null;
    notes = '';

    constructor(
        private svcData: DataService
    ) { }

    ngOnInit(): void {
        console.log('Listing Summary ngOnInit');
        this.rec = this.svcData.getObject('listing');
        console.log(this.rec);

        if (this.rec) {
            this.notes = this.rec.notes ? this.rec.notes.replace('\n', '<br>') : '';
        }
    }

}
