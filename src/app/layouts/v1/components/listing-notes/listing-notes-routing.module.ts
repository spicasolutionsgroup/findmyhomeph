import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingNotesComponent } from './listing-notes.component';

const routes: Routes = [{ path: '', component: ListingNotesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingNotesRoutingModule { }
