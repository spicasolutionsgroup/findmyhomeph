import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchListPagerComponent } from './search-list-pager.component';

describe('SearchListPagerComponent', () => {
  let component: SearchListPagerComponent;
  let fixture: ComponentFixture<SearchListPagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchListPagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchListPagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
