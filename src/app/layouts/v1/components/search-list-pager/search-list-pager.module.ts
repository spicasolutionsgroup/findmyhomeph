import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchListPagerRoutingModule } from './search-list-pager-routing.module';
import { SearchListPagerComponent } from './search-list-pager.component';


@NgModule({
  declarations: [SearchListPagerComponent],
  imports: [
    CommonModule,
    SearchListPagerRoutingModule
  ],
  exports: [
    SearchListPagerComponent
  ]
})
export class SearchListPagerModule { }
