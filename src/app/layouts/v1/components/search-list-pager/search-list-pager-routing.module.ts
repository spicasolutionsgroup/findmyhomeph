import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchListPagerComponent } from './search-list-pager.component';

const routes: Routes = [{ path: '', component: SearchListPagerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchListPagerRoutingModule { }
