import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPageRoutingModule } from './search-page-routing.module';
import { SearchPageComponent } from './search-page.component';

// Custom Modules
// import { TopbarModule } from '../topbar/topbar.module';
import { SearchFilterModule } from '../search-filter/search-filter.module';
import { SearchListModule } from '../search-list/search-list.module';
// import { TaskmenuFloatModule } from './taskmenu-float/taskmenu-float.module';

@NgModule({
  declarations: [SearchPageComponent],
  imports: [
    CommonModule,
    SearchPageRoutingModule,

    SearchFilterModule,
    SearchListModule
  ]
})
export class SearchPageModule { }
