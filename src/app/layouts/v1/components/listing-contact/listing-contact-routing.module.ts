import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingContactComponent } from './listing-contact.component';

const routes: Routes = [{ path: '', component: ListingContactComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingContactRoutingModule { }
