import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingContactRoutingModule } from './listing-contact-routing.module';
import { ListingContactComponent } from './listing-contact.component';


@NgModule({
    declarations: [ListingContactComponent],
    imports: [
        CommonModule,
        ListingContactRoutingModule
    ],
    exports: [
        ListingContactComponent
    ]
})
export class ListingContactModule { }
