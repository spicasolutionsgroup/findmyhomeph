import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingContactComponent } from './listing-contact.component';

describe('ListingContactComponent', () => {
  let component: ListingContactComponent;
  let fixture: ComponentFixture<ListingContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
