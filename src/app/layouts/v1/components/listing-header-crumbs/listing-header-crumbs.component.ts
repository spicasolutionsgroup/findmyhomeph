import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listing-header-crumbs',
  templateUrl: './listing-header-crumbs.component.html',
  styleUrls: ['./listing-header-crumbs.component.css']
})
export class ListingHeaderCrumbsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
