import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingHeaderCrumbsRoutingModule } from './listing-header-crumbs-routing.module';
import { ListingHeaderCrumbsComponent } from './listing-header-crumbs.component';


@NgModule({
    declarations: [ListingHeaderCrumbsComponent],
    imports: [
        CommonModule,
        ListingHeaderCrumbsRoutingModule
    ],
    exports: [
        ListingHeaderCrumbsComponent
    ]
})
export class ListingHeaderCrumbsModule { }
