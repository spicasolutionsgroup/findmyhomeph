import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingHeaderCrumbsComponent } from './listing-header-crumbs.component';

describe('ListingHeaderCrumbsComponent', () => {
  let component: ListingHeaderCrumbsComponent;
  let fixture: ComponentFixture<ListingHeaderCrumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingHeaderCrumbsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingHeaderCrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
