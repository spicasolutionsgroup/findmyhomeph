import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingHeaderCrumbsComponent } from './listing-header-crumbs.component';

const routes: Routes = [{ path: '', component: ListingHeaderCrumbsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingHeaderCrumbsRoutingModule { }
