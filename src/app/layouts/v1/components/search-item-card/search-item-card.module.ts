import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchItemCardRoutingModule } from './search-item-card-routing.module';
import { SearchItemCardComponent } from './search-item-card.component';


@NgModule({
  declarations: [SearchItemCardComponent],
  imports: [
    CommonModule,
    SearchItemCardRoutingModule
  ],
  exports: [
      SearchItemCardComponent
  ]
})
export class SearchItemCardModule { }
