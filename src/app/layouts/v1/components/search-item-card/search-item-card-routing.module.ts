import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchItemCardComponent } from './search-item-card.component';

const routes: Routes = [{ path: '', component: SearchItemCardComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchItemCardRoutingModule { }
