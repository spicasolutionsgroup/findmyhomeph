import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingSimilarComponent } from './listing-similar.component';

describe('ListingSimilarComponent', () => {
  let component: ListingSimilarComponent;
  let fixture: ComponentFixture<ListingSimilarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingSimilarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingSimilarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
