import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingSimilarRoutingModule } from './listing-similar-routing.module';
import { ListingSimilarComponent } from './listing-similar.component';


@NgModule({
  declarations: [ListingSimilarComponent],
  imports: [
    CommonModule,
    ListingSimilarRoutingModule
  ],
  exports: [
      ListingSimilarComponent
  ]
})
export class ListingSimilarModule { }
