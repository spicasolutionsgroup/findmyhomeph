import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingSimilarComponent } from '../listing-similar/listing-similar.component';

const routes: Routes = [{ path: '', component: ListingSimilarComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingSimilarRoutingModule { }
