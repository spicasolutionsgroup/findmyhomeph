import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsListingContactRoutingModule } from './details-listing-contact-routing.module';
import { DetailsListingContactComponent } from './details-listing-contact.component';


@NgModule({
    declarations: [DetailsListingContactComponent],
    imports: [
        CommonModule,
        DetailsListingContactRoutingModule
    ],
    exports: [
        DetailsListingContactComponent
    ]
})
export class DetailsListingContactModule { }
