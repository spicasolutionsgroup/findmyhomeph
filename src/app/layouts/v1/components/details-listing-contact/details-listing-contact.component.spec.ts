import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsListingContactComponent } from './details-listing-contact.component';

describe('DetailsListingContactComponent', () => {
  let component: DetailsListingContactComponent;
  let fixture: ComponentFixture<DetailsListingContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsListingContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsListingContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
