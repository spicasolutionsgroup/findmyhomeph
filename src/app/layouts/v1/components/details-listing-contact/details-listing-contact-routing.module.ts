import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsListingContactComponent } from './details-listing-contact.component';

const routes: Routes = [{ path: '', component: DetailsListingContactComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsListingContactRoutingModule { }
