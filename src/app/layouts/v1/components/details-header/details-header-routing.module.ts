import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsHeaderComponent } from './details-header.component';

const routes: Routes = [{ path: '', component: DetailsHeaderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsHeaderRoutingModule { }
