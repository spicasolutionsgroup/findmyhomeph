import { Component, OnInit } from '@angular/core';
import { UiService } from '../../../../services/ui.service';

@Component({
    selector: 'app-details-header',
    templateUrl: './details-header.component.html',
    styleUrls: ['./details-header.component.css']
})
export class DetailsHeaderComponent implements OnInit {

    svcUI: UiService;

    constructor(private uiSvc: UiService) {
        this.svcUI = uiSvc;
    }

    ngOnInit(): void {
    }

    backToSearch() {
        this.svcUI.navigateTo('/v1/search');
    }

}
