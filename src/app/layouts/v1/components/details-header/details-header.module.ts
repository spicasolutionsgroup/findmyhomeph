import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsHeaderRoutingModule } from './details-header-routing.module';
import { DetailsHeaderComponent } from './details-header.component';

@NgModule({
    declarations: [DetailsHeaderComponent],
    imports: [
        CommonModule,
        DetailsHeaderRoutingModule
    ],
    exports: [
        DetailsHeaderComponent
    ]
})
export class DetailsHeaderModule { }
