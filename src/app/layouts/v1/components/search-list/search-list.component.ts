import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
// import { SearchItemComponent } from '../search-item/search-item.component';
// import { ApiService } from '../../../../services/api.service';
import { UiService } from '../../../../services/ui.service';
// import { DataService } from '../../../../services/data.service';

import { ListingView } from '../../../../models/listing-view.model';

@Component({
    selector: 'app-search-list',
    templateUrl: './search-list.component.html',
    styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {

    list = [];
    subList: Subscription;

    constructor(
        private route: ActivatedRoute,
        private svcUI: UiService
        // private svcAPI: ApiService
        // private svcData: DataService
    ) {}

    getDuration(rec) {
        let today = new Date();
        console.log(`today: ${today}`);
        console.log(`date: ${rec.datecreated}`);
        let date = new Date(rec.datecreated);
        console.log(`date: ${date}`);

        let diff = today.getTime() - date.getTime();
        let days = Math.floor(diff / (1000 * 60 * 60 *24));
        diff -= days * (1000 * 60 * 60 * 24);

        let hours = Math.floor(diff / (1000 * 60 * 60));
        diff -= hours * (1000 * 60 * 60);

        console.log(`days = ${days}, hours = ${hours}`);

        if (days > 1) {
            if (days == 1) {
                return 'a day ago';
            }
            else if (days <= 30) {
                return `${days} days ago`
            }
            else {
                return 'more than a month ago'
            }
        }
        else {
            if (hours < 1) {
                return 'just now';
            }
            else if (hours < 24) {
                return `${hours} hours ago`;
            }
        }
    }

    ngOnInit(): void {
        console.log('Search List ngOnInit()');
        this.subList = this.route.data.pipe(map((res) => {
            return res.list;
        })).subscribe((data) => {
            console.log(data);
            this.list = data as ListingView[];
            this.list = this.list.map(x => {
                x.addrval = '';
                if (x.address) {
                    x.addrval += `${x.address}, `;
                }
                if (x.brgy) {
                    x.addrval += `${x.brgy}, `;
                }
                if (x.city) {
                    x.addrval += `${x.city}`;
                }
                if (x.province) {
                    x.addrval += `, ${x.province}`;
                }

                x.age = this.getDuration(x);
                console.log(`age = ${x.age}`);
                return x;
            });
            console.log(this.list);
        });

        console.log(this.list);
    }

    viewDetails(id) {
        this.svcUI.navigateTo('/v1/listing', {id: id});
    }

}
