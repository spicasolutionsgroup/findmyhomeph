import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchListRoutingModule } from './search-list-routing.module';
import { SearchListComponent } from './search-list.component';

// Custom Modules
// import { TopbarModule } from '../topbar/topbar.module';
import { SearchItemCardModule } from '../search-item-card/search-item-card.module';
import { SearchListPagerModule } from '../search-list-pager/search-list-pager.module';
// import { TaskmenuFloatModule } from './taskmenu-float/taskmenu-float.module';

@NgModule({
    declarations: [SearchListComponent],
    imports: [
        CommonModule,
        SearchListRoutingModule,

        // Custom Modules
        SearchItemCardModule,
        SearchListPagerModule
    ],
    exports: [
        SearchListComponent
    ]
})
export class SearchListModule { }
