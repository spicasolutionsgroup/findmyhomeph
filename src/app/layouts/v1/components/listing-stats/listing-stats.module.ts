import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingStatsRoutingModule } from './listing-stats-routing.module';
import { ListingStatsComponent } from './listing-stats.component';


@NgModule({
  declarations: [ListingStatsComponent],
  imports: [
    CommonModule,
    ListingStatsRoutingModule
  ],
  exports: [
      ListingStatsComponent
  ]
})
export class ListingStatsModule { }
