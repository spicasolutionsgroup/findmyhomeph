import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStatsComponent } from './listing-stats.component';

describe('ListingStatsComponent', () => {
  let component: ListingStatsComponent;
  let fixture: ComponentFixture<ListingStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
