import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingStatsComponent } from './listing-stats.component';

const routes: Routes = [{ path: '', component: ListingStatsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingStatsRoutingModule { }
