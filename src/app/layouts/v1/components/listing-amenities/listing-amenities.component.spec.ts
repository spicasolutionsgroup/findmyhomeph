import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingAmenitiesComponent } from './listing-amenities.component';

describe('ListingAmenitiesComponent', () => {
  let component: ListingAmenitiesComponent;
  let fixture: ComponentFixture<ListingAmenitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingAmenitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingAmenitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
