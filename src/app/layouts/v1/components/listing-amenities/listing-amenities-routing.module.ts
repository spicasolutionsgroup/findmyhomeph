import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingAmenitiesComponent } from './listing-amenities.component';

const routes: Routes = [{ path: '', component: ListingAmenitiesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingAmenitiesRoutingModule { }
