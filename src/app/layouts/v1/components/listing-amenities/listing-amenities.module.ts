import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingAmenitiesRoutingModule } from './listing-amenities-routing.module';
import { ListingAmenitiesComponent } from './listing-amenities.component';


@NgModule({
    declarations: [ListingAmenitiesComponent],
    imports: [
        CommonModule,
        ListingAmenitiesRoutingModule
    ],
    exports: [
        ListingAmenitiesComponent
    ]
})
export class ListingAmenitiesModule { }
