import { Component, OnInit } from '@angular/core';
import { UiService } from '../../../../services/ui.service';

@Component({
    selector: 'app-listing-header',
    templateUrl: './listing-header.component.html',
    styleUrls: ['./listing-header.component.css']
})
export class ListingHeaderComponent implements OnInit {

    svcUI: UiService;

    constructor(private uiSvc: UiService) {
        this.svcUI = uiSvc;
    }

    ngOnInit(): void {
    }

    backToSearch() {
        this.svcUI.navigateTo('/v1/search');
    }

}
