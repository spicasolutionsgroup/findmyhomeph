import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingHeaderRoutingModule } from './listing-header-routing.module';
import { ListingHeaderComponent } from './listing-header.component';

import { ListingHeaderCrumbsModule } from '../listing-header-crumbs/listing-header-crumbs.module';

@NgModule({
    declarations: [ListingHeaderComponent],
    imports: [
        CommonModule,
        ListingHeaderRoutingModule,

        ListingHeaderCrumbsModule
    ],
    exports: [
        ListingHeaderComponent
    ]
})
export class ListingHeaderModule { }
