import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingHeaderComponent } from './listing-header.component';

const routes: Routes = [{ path: '', component: ListingHeaderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingHeaderRoutingModule { }
