import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingPageRoutingModule } from './listing-page-routing.module';
import { ListingPageComponent } from './listing-page.component';

import { ListingHeaderModule } from '../listing-header/listing-header.module';
import { ListingGalleryModule } from '../listing-gallery/listing-gallery.module';
import { ListingStatsModule } from '../listing-stats/listing-stats.module';
import { ListingSummaryModule } from '../listing-summary/listing-summary.module';
import { ListingNotesModule } from '../listing-notes/listing-notes.module';
import { ListingAmenitiesModule } from '../listing-amenities/listing-amenities.module';
import { ListingDetailsModule } from '../listing-details/listing-details.module';
import { ListingSimilarModule } from '../listing-similar/listing-similar.module';
import { ListingContactModule } from '../listing-contact/listing-contact.module';
import { ListingOverlaysModule } from '../listing-overlays/listing-overlays.module';

@NgModule({
    declarations: [ListingPageComponent],
    imports: [
        CommonModule,
        ListingPageRoutingModule,

        ListingHeaderModule,
        ListingGalleryModule,
        ListingStatsModule,
        ListingSummaryModule,
        ListingNotesModule,
        ListingAmenitiesModule,
        ListingDetailsModule,
        ListingOverlaysModule,
        ListingSimilarModule,

        ListingContactModule
    ],
    exports: [
        ListingPageComponent
    ]
})
export class ListingPageModule { }
