import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import { Listing } from '../../../../models/listing.model';

@Component({
    selector: 'app-listing-page',
    templateUrl: './listing-page.component.html',
    styleUrls: ['./listing-page.component.css']
})
export class ListingPageComponent implements OnInit {

    rec = null;
    subList: Subscription;

    constructor(
        private route: ActivatedRoute,
        private svcData: DataService
    ) { }

    ngOnInit(): void {
        console.log('Listing Page ngOnInit()');
        this.subList = this.route.data.pipe(map((res) => {
            return res.record;
        })).subscribe((data) => {
            this.rec = data as Listing;
            console.log(this.rec);
            this.svcData.setObject('listing', this.rec);
        });
    }

}
