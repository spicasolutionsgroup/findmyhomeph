import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingGalleryRoutingModule } from './listing-gallery-routing.module';
import { ListingGalleryComponent } from './listing-gallery.component';

@NgModule({
    declarations: [ListingGalleryComponent],
    imports: [
        CommonModule,
        ListingGalleryRoutingModule
    ],
    exports: [
        ListingGalleryComponent
    ]
})
export class ListingGalleryModule { }
