import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../services/data.service';

@Component({
  selector: 'app-listing-gallery',
  templateUrl: './listing-gallery.component.html',
  styleUrls: ['./listing-gallery.component.css']
})
export class ListingGalleryComponent implements OnInit {

    rec = null;

    constructor(
        private svcData: DataService
    ) { }

    getAddrVal() {
        let val = '';
        if (this.rec.address.address_1) {
            val += `${this.rec.address.address_1}, `;
        }
        // if (this.rec.brgy) {
        //     val += `${this.rec.brgy}, `;
        // }
        if (this.rec.address.city) {
            val += `${this.rec.address.city}`;
        }
        if (this.rec.address.province) {
            val += `, ${this.rec.address.province}`;
        }
        return val;
    }

    ngOnInit(): void {
        console.log('Listing Gallery ngOnInit');
        this.rec = this.svcData.getObject('listing');
        console.log(this.rec);

        this.rec.addrval = this.getAddrVal();

        if (this.rec.isForRent == true || this.rec.isRentToOwn == true) {
            this.rec.isRental = true;
        }
    }

}
