import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingGalleryComponent } from './listing-gallery.component';

describe('ListingGalleryComponent', () => {
  let component: ListingGalleryComponent;
  let fixture: ComponentFixture<ListingGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
