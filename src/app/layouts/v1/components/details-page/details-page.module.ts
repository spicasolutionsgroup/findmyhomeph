import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsPageRoutingModule } from './details-page-routing.module';
import { DetailsPageComponent } from './details-page.component';

import { DetailsHeaderModule } from '../details-header/details-header.module';
import { DetailsPageCarouselModule } from '../details-page-carousel/details-page-carousel.module';
import { DetailsPageDetailsModule } from '../details-page-details/details-page-details.module';
import { DetailsListingContactModule } from '../details-listing-contact/details-listing-contact.module';
// import { DetailsSidebarSummaryModule } from '../details-sidebar-summary/details-sidebar-summary.module';

@NgModule({
    declarations: [DetailsPageComponent],
    imports: [
        CommonModule,
        DetailsPageRoutingModule,

        DetailsHeaderModule,
        DetailsPageCarouselModule,
        DetailsPageDetailsModule,
        DetailsListingContactModule
        // DetailsSidebarSummaryModule
    ]
})
export class DetailsPageModule { }
