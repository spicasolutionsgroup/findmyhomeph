import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateHeaderRoutingModule } from './create-header-routing.module';
import { CreateHeaderComponent } from './create-header.component';


@NgModule({
    declarations: [CreateHeaderComponent],
    imports: [
        CommonModule,
        CreateHeaderRoutingModule
    ],
    exports: [
        CreateHeaderComponent
    ]
})
export class CreateHeaderModule { }
