import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateHeaderComponent } from './create-header.component';

const routes: Routes = [{ path: '', component: CreateHeaderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateHeaderRoutingModule { }
