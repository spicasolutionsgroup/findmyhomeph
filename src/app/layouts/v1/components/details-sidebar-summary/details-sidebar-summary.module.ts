import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsSidebarSummaryRoutingModule } from './details-sidebar-summary-routing.module';
import { DetailsSidebarSummaryComponent } from './details-sidebar-summary.component';


@NgModule({
    declarations: [DetailsSidebarSummaryComponent],
    imports: [
        CommonModule,
        DetailsSidebarSummaryRoutingModule
    ],
    exports: [
        DetailsSidebarSummaryComponent
    ]
})
export class DetailsSidebarSummaryModule { }
