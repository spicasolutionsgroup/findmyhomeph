import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsSidebarSummaryComponent } from './details-sidebar-summary.component';

const routes: Routes = [{ path: '', component: DetailsSidebarSummaryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsSidebarSummaryRoutingModule { }
