import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsSidebarSummaryComponent } from './details-sidebar-summary.component';

describe('DetailsSidebarSummaryComponent', () => {
  let component: DetailsSidebarSummaryComponent;
  let fixture: ComponentFixture<DetailsSidebarSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsSidebarSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsSidebarSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
