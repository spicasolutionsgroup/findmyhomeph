import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsPageDetailsRoutingModule } from './details-page-details-routing.module';
import { DetailsPageDetailsComponent } from './details-page-details.component';


@NgModule({
    declarations: [DetailsPageDetailsComponent],
    imports: [
        CommonModule,
        DetailsPageDetailsRoutingModule
    ],
    exports: [
        DetailsPageDetailsComponent
    ]
})
export class DetailsPageDetailsModule { }
