import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsPageDetailsComponent } from './details-page-details.component';

describe('DetailsPageDetailsComponent', () => {
  let component: DetailsPageDetailsComponent;
  let fixture: ComponentFixture<DetailsPageDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsPageDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsPageDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
