import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsPageDetailsComponent } from './details-page-details.component';

const routes: Routes = [{ path: '', component: DetailsPageDetailsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsPageDetailsRoutingModule { }
