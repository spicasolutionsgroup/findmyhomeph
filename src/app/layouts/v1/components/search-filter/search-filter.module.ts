import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchFilterRoutingModule } from './search-filter-routing.module';
import { SearchFilterComponent } from './search-filter.component';


@NgModule({
  declarations: [SearchFilterComponent],
  imports: [
    CommonModule,
    SearchFilterRoutingModule
  ],
  exports: [SearchFilterComponent]
})
export class SearchFilterModule { }
