import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingOverlaysComponent } from './listing-overlays.component';

const routes: Routes = [{ path: '', component: ListingOverlaysComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingOverlaysRoutingModule { }
