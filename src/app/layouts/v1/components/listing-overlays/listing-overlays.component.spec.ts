import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingOverlaysComponent } from './listing-overlays.component';

describe('ListingOverlaysComponent', () => {
  let component: ListingOverlaysComponent;
  let fixture: ComponentFixture<ListingOverlaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingOverlaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingOverlaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
