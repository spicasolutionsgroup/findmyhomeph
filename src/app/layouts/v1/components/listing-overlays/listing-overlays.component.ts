import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { GoogleMap, MapMarker, MapInfoWindow } from '@angular/google-maps';
// import { localizedString } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'app-listing-overlays',
    templateUrl: './listing-overlays.component.html',
    styleUrls: ['./listing-overlays.component.css']
})
export class ListingOverlaysComponent implements OnInit {

    rec = null;
    lat: number;
    lng: number;
    center: google.maps.LatLngLiteral;

    constructor(
        private svcData: DataService
    ) { }

    ngOnInit(): void {
        console.log('Listing Overlays ngOnInit');
        this.rec = this.svcData.getObject('listing');
        console.log(this.rec);

        if (this.rec) {
            this.lat = Number(this.rec.lat);
            this.lng = Number(this.rec.long);
            this.center = { lat: this.lat, lng: this.lng };
        }
        console.log(this.center);

        const map = new google.maps.Map(
            document.getElementById("map") as HTMLElement,
            {
                zoom: 16,
                center: this.center,
                zoomControl: true,
                scrollwheel: true,
                disableDoubleClickZoom: false,
                streetViewControl: false,
                fullscreenControl: false,
                mapTypeControl: false
            }
        );

        const marker = new google.maps.Marker({
            position: this.center,
            map: map
        });

    }

}
