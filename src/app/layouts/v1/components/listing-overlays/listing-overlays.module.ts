import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingOverlaysRoutingModule } from './listing-overlays-routing.module';
import { ListingOverlaysComponent } from './listing-overlays.component';

import { GoogleMapsModule } from '@angular/google-maps';


@NgModule({
    declarations: [ListingOverlaysComponent],
    imports: [
        CommonModule,
        ListingOverlaysRoutingModule,

        GoogleMapsModule
    ],
    exports: [
        ListingOverlaysComponent
    ]
})
export class ListingOverlaysModule { }
