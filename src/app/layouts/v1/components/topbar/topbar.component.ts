import { Component, OnInit } from '@angular/core';
import { UiService } from '../../../../services/ui.service';

@Component({
    selector: 'app-topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

    svcUI: UiService;

    constructor(private uiSvc: UiService) {
        this.svcUI = uiSvc;
    }

    ngOnInit(): void {
    }

    navTo(page) {
        this.svcUI.navigateTo(`/v1/${page}`);
    }

}
