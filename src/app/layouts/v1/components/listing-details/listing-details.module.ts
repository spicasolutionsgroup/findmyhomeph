import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingDetailsRoutingModule } from './listing-details-routing.module';
import { ListingDetailsComponent } from './listing-details.component';

import { ListingStatsModule } from '../listing-stats/listing-stats.module';
import { ListingAmenitiesModule } from '../listing-amenities/listing-amenities.module';
import { ListingOverlaysModule } from '../listing-overlays/listing-overlays.module';

@NgModule({
    declarations: [ListingDetailsComponent],
    imports: [
        CommonModule,
        ListingDetailsRoutingModule,

        ListingStatsModule,
        ListingAmenitiesModule,
        ListingOverlaysModule
    ],
    exports: [
        ListingDetailsComponent
    ]
})
export class ListingDetailsModule { }
