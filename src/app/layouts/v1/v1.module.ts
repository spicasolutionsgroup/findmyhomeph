import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { V1RoutingModule } from './v1-routing.module';
import { V1Component } from './v1.component';

import { TopbarModule } from './components/topbar/topbar.module';
import { FooterModule } from './components/footer/footer.module';

@NgModule({
  declarations: [V1Component],
  imports: [
    CommonModule,
    V1RoutingModule,
    TopbarModule,
    FooterModule
  ]
})
export class V1Module { }
