import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { V1Component } from './v1.component';
import { ListResolver } from '../../resolvers/list.resolver';
import { RecordResolver } from '../../resolvers/record.resolver';

const routes: Routes = [
    {
        path: '', component: V1Component, children: [
            { path: '', redirectTo: 'search', pathMatch: 'full' },
            {
                path: 'search',
                loadChildren: async () => (await import('./components/search-page/search-page.module')).SearchPageModule,
                resolve: { list: ListResolver },
                data: { collection: 'listings' }
            },
            {
                path: 'listing',
                loadChildren: async () => (await import('./components/listing-page/listing-page.module')).ListingPageModule,
                resolve: { record: RecordResolver },
                data: { collection: 'listings' }
            },
            { path: 'create', loadChildren: async () => (await import('./components/create-page/create-page.module')).CreatePageModule },
            { path: 'login', loadChildren: async () => (await import('./components/create-page/create-page.module')).CreatePageModule }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class V1RoutingModule { }
