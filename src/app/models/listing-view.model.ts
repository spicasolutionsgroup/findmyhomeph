export interface ListingView {
    _id: string;
    address: string;
    brgy: string;
    city: string;
    province: string;
    price: number;
    beds: number;
    baths: number;
    floor: number;
    parking: number;
    images: string[];
}