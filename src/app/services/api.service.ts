import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    SVC_URL = 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/findmyhome-matpf/service/findmyhome/incoming_webhook/';
    fetchDone:boolean = true;
    fetchDoneChange: Subject<boolean> = new Subject<boolean>();

    constructor(private http: HttpClient) { }

    setFetchDone(d) {
        this.fetchDone = d;
        this.fetchDoneChange.next(this.fetchDone);
    }

    onFetchDoneChange(): Observable<boolean> {
        return this.fetchDoneChange.asObservable();
    }

    public post(action: string, type: string, params?: object, body?: object): Observable<any> {
        const query = [];
        if (params) {
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    query.push(`${i}=${params[i]}`);
                }
            }
        }

        const url = `${this.SVC_URL}${action}?t=${type}&${query.join('&')}`;
        console.log(`+++ ${action} url: ${url}`);
        return this.http.post(this.addUrlTS(url), body ? body : {});
    }

    private addUrlTS(url: string): string {
        return `${url}&ttl=${(new Date()).getTime()}`;
    }

}
