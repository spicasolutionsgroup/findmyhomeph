import { Injectable } from '@angular/core';
import { ObjectUnsubscribedErrorCtor } from 'rxjs/internal/util/ObjectUnsubscribedError';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    appData = {};

    constructor() { }

    public setObject(key: string, val: object) {
        console.log(`setObject(${key})`);
        console.log(val);
        this.appData[key] = val;
    }

    public getObject(key: string) {
        console.log(`getObject(${key})`);
        console.log(this.appData[key]);
        return this.appData[key];
    }
}
