import { Injectable } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UiService {

    router: Router;
    route: ActivatedRoute;

    constructor(private _router: Router, private _route: ActivatedRoute) {
        this.router = _router;
        this.route = _route;
    }

    public navigateTo(page, parms?) {
        var url = `${page}`;

        if (parms) {
            url += '?';
            for (let [k, v] of Object.entries(parms)) {
                url += `${k}=${v}&`;
            }
        }

        console.log(`url = ${url}`);
        this.router.navigateByUrl(url);
    }
}
