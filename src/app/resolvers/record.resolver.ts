import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class RecordResolver implements Resolve<any> {

    constructor(
        private svcApi: ApiService
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        console.log(`V1-RECORD RESOLVER, type=${route.data['listtype']}`);
        const id = route.queryParamMap.get('id');
        return this.svcApi.post('get', route.data['collection'], { id: id });
    }
}