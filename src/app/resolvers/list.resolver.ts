import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class ListResolver implements Resolve<any> {

    constructor(
        private svcApi: ApiService
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        console.log(`LIST RESOLVER, type=${route.data['collection']}`);
        return this.svcApi.post('find', route.data['collection']);
    }
}